FROM node:14-alpine as base

ARG API_URL='https://api.beer-alerts.com'

# This needs to be set before yarn build is run or it won't be included!
ENV REACT_APP_API_URL=$API_URL

WORKDIR /app

COPY package.json .
COPY yarn.lock .

RUN yarn --pure-lockfile --network-timeout 500000

COPY tsconfig.json .

COPY src ./src
COPY public ./public

RUN ["yarn", "build"]
RUN yarn install --production

FROM node:14-alpine as prod

WORKDIR /app

COPY --from=base /app/package.json .
COPY --from=base /app/build ./build
COPY --from=base /app/node_modules ./node_modules

CMD ["npx", "serve", "-l", "80", "-s", "build"]