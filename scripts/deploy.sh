#!/bin/bash

(
  set -e
  profile="$1"
  account_number=$(aws sts get-caller-identity --profile $profile --query "Account" | xargs)

  aws ecr get-login-password --region us-west-2 --profile $profile | docker login --username AWS --password-stdin $account_number.dkr.ecr.us-west-2.amazonaws.com
  docker build \
  --cache-from $account_number.dkr.ecr.us-west-2.amazonaws.com/ba-website-repository:latest \
  -t $account_number.dkr.ecr.us-west-2.amazonaws.com/ba-website-repository:$(git rev-parse HEAD) \
  -t $account_number.dkr.ecr.us-west-2.amazonaws.com/ba-website-repository:latest \
  --target prod .

  docker push $account_number.dkr.ecr.us-west-2.amazonaws.com/ba-website-repository:$(git rev-parse HEAD)
  docker push $account_number.dkr.ecr.us-west-2.amazonaws.com/ba-website-repository:latest

  aws ecs update-service \
  --profile $profile \
  --cluster ba-cluster \
  --service ba-website-service \
  --force-new-deployment
)