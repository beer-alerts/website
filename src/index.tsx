import { initializeApp } from 'firebase/app';
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import App from './components/App';
import LoadingDisplay from './components/App/LoadingDisplay';
import AuthDataProvider from './components/AuthDataProvider';

import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

initializeApp({
  apiKey: 'AIzaSyBxP1N1grBYljJD3NfZ5wk_YJ9YiGs3ZWQ',
  authDomain: 'beer-alerts.firebaseapp.com',
  databaseURL: 'https://beer-alerts.firebaseio.com',
  projectId: 'beer-alerts',
  storageBucket: 'beer-alerts.appspot.com',
  messagingSenderId: '884726079486',
  appId: '1:884726079486:web:60c81907131a3f9bf2eaaf',
});

ReactDOM.render(
  <React.Suspense fallback={<LoadingDisplay />}>
    <React.StrictMode>
      <BrowserRouter>
        <AuthDataProvider>
          <App />
        </AuthDataProvider>
      </BrowserRouter>
    </React.StrictMode>
  </React.Suspense>,
  document.getElementById('root'),
);
