import axios from 'axios';

const { REACT_APP_API_URL: API_URL } = process.env;

const api = axios.create({ baseURL: API_URL });

export interface RequestOptions {
  body?: object;
  bearerToken: string;
}

export const GET_WITH_STATUS = <T>(
  path: string,
  options?: RequestOptions,
): Promise<{ statusCode: number; data: T }> =>
  api
    .get(path, { headers: { Authorization: `Bearer: ${options?.bearerToken}` } })
    .then(({ status, data }) => ({ statusCode: status, data: data.data }));

export const GET = <T>(path: string, options?: RequestOptions): Promise<T> =>
  api
    .get(path, { headers: { Authorization: `Bearer: ${options?.bearerToken}` } })
    .then(({ data }) => data.data);

export const POST = <T>(path: string, options?: RequestOptions): Promise<T> =>
  api
    .post(path, options?.body, { headers: { Authorization: `Bearer: ${options?.bearerToken}` } })
    .then(({ data }) => data.data);

export const PATCH = <T>(path: string, options?: RequestOptions): Promise<T> =>
  api
    .patch(path, options?.body, { headers: { Authorization: `Bearer: ${options?.bearerToken}` } })
    .then(({ data }) => data.data);

export const PUT = <T>(path: string, options?: RequestOptions): Promise<T> =>
  api
    .put(path, options?.body, { headers: { Authorization: `Bearer: ${options?.bearerToken}` } })
    .then(({ data }) => data.data);

export const DELETE = <T>(path: string, options?: RequestOptions): Promise<T> =>
  api
    .delete(path, { headers: { Authorization: `Bearer: ${options?.bearerToken}` } })
    .then(({ data }) => data.data);
