import React from 'react';

import './WebsiteButton.css';

interface WebsiteButtonProps {
  text: string;
  type?: 'positive' | 'negative' | 'neutral';
  enabled?: boolean;
  onClick: () => void | Promise<void>;
}

export function WebsiteButton({
  text,
  onClick,
  type = 'neutral',
  enabled = true,
}: WebsiteButtonProps) {
  const className = type === 'neutral' ? 'websiteButton' : `websiteButton ${type}`;
  return (
    <button className={className} disabled={!enabled} onClick={onClick}>
      {text}
    </button>
  );
}
