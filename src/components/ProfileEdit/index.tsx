import { AxiosError } from 'axios';
import React, { useEffect, useState } from 'react';

import Spinner from 'react-bootstrap/Spinner';
import { DataEntry } from '../DataEntry';
import { WebsiteButton } from '../WebsiteButton';

import './index.css';

type UserInfo = { name?: string; email?: string };
type Error = { code?: string; message?: string };

type ProfileEditProps = {
  originalDisplayName: string;
  originalEmail: string;
  onSaveClicked: (userInfo: UserInfo) => void | Promise<void>;
};

export default function ProfileEdit({
  originalDisplayName,
  originalEmail,
  onSaveClicked,
}: ProfileEditProps) {
  const [disableForm, setDisableForm] = useState(false);
  const [error, setError] = useState<Error>({});

  const [updatedUserData, setUpdatedUserData] = useState<UserInfo>({
    name: originalDisplayName,
    email: originalEmail,
  });
  useEffect(() => {
    setUpdatedUserData({ name: originalDisplayName, email: originalEmail });
  }, [originalDisplayName, originalEmail]);

  return (
    <div className="profileEditMainContainer">
      <div className="profileEditTitleContainer">
        <div>Subscription Info</div>
        <div className="profileEditSpinnerContainer">
          <Spinner hidden={!disableForm} animation="border" role="status" />
        </div>
      </div>
      <div>
        <DataEntry
          title="Display Name:"
          initialValue={updatedUserData.name}
          enabled={!disableForm}
          onInput={updatedName => {
            setUpdatedUserData({ ...updatedUserData, name: updatedName });
          }}
        />
        <DataEntry
          title="Alert Email:"
          inputMode="email"
          initialValue={updatedUserData.email}
          enabled={!disableForm}
          onInput={updatedEmail => {
            setUpdatedUserData({ ...updatedUserData, email: updatedEmail });
          }}
        />
      </div>
      <div className="profileEditSubmitContainer">
        {error.code ? (
          <div className="profileEditErrorDisplayContainer">
            <p>{`Code: ${error.code}`}</p>
            <p>{`Message: ${error.message}`}</p>
          </div>
        ) : null}
        <div className="profileEditButtonContainer">
          <WebsiteButton
            text="Save"
            type="positive"
            enabled={
              !disableForm &&
              hasInfoChanged({ name: originalDisplayName, email: originalEmail }, updatedUserData)
            }
            onClick={async () => {
              setError({});
              setDisableForm(true);
              try {
                await onSaveClicked(
                  extractChangedFields(
                    { name: originalDisplayName, email: originalEmail },
                    updatedUserData,
                  ),
                );
              } catch (err) {
                const { status, data } = (err as AxiosError).response!;
                setError({
                  code: `${status}`,
                  message: data?.error ?? 'Unexpected error occurred',
                });
              } finally {
                setDisableForm(false);
              }
            }}
          />
        </div>
      </div>
    </div>
  );
}

function hasInfoChanged(originalInfo: UserInfo, updatedInfo: UserInfo): boolean {
  return (
    !!updatedInfo.name &&
    !!updatedInfo.email &&
    (updatedInfo.name !== originalInfo.name || updatedInfo.email !== originalInfo.email)
  );
}

function extractChangedFields(originalInfo: UserInfo, updatedInfo: UserInfo): UserInfo {
  const finalInfo: UserInfo = {};
  if ((updatedInfo.name ?? '').length > 0 && updatedInfo.name !== originalInfo.name) {
    finalInfo.name = updatedInfo.name;
  }
  if ((updatedInfo.email ?? '').length > 0 && updatedInfo.email !== originalInfo.email) {
    finalInfo.email = updatedInfo.email;
  }

  return finalInfo;
}
