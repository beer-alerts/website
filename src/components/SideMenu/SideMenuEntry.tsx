import React from 'react';
import { useHistory } from 'react-router-dom';

import './SideMenuEntry.css';

interface NavigationProps {
  location: string;
}

interface ActionProps {
  onClick: () => void | Promise<void>;
}

type SideMenuEntryProps = (NavigationProps | ActionProps) & {
  onBeforeAction: () => void;
  label: string;
};

export function SideMenuEntry(props: SideMenuEntryProps) {
  const history = useHistory();

  if (isNavigationType(props)) {
    return (
      <div className="sideMenuEntry">
        <div
          onClick={() => {
            props.onBeforeAction();
            history.push(props.location);
          }}
        >
          {props.label}
        </div>
      </div>
    );
  }
  return (
    <div className="sideMenuEntry">
      <div
        onClick={() => {
          props.onBeforeAction();
          props.onClick();
        }}
      >
        {props.label}
      </div>
    </div>
  );
}

function isNavigationType(props: NavigationProps | ActionProps): props is NavigationProps {
  return !!(props as NavigationProps).location;
}
