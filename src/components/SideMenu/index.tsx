import React from 'react';
import { useAuthData } from '../AuthDataProvider';
import DimmingOverlay from '../DimmingOverlay';

import './SideMenu.css';
import { SideMenuEntry } from './SideMenuEntry';

interface SideMenuProps {
  isOpen: boolean;
  onDismiss: () => void;
}

export function SideMenu({ isOpen, onDismiss }: SideMenuProps) {
  const { logout } = useAuthData();

  return (
    <DimmingOverlay isOpen={isOpen} overlayTapped={onDismiss}>
      <div
        className={`sideMenuMainContainer ${isOpen ? 'open' : 'closed'}`}
        onClick={e => e.stopPropagation()}
      >
        <div className="sideMenuContentContainer">
          <div className="sideMenuSection">
            <div className="sideMenuSectionSeparator" />
            <SideMenuEntry
              onBeforeAction={onDismiss}
              label="Subscription Management"
              location="/"
            />
            <SideMenuEntry onBeforeAction={onDismiss} label="Purchase Beer" location="/buy" />
            <SideMenuEntry onBeforeAction={onDismiss} label="Edit Your Info" location="/profile" />
            <div className="sideMenuSectionSeparator" />
          </div>
          <div className="sideMenuSection">
            <div className="sideMenuSectionSeparator" />
            <SideMenuEntry onBeforeAction={onDismiss} label="Logout" onClick={logout} />
            <div className="sideMenuSectionSeparator" />
          </div>
        </div>
      </div>
    </DimmingOverlay>
  );
}
