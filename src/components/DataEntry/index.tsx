import React from 'react';

import './DataEntry.css';

interface DataEntryProps {
  title: string;
  inputMode?: 'none' | 'text' | 'tel' | 'url' | 'email' | 'numeric' | 'decimal' | 'search';
  initialValue?: string;
  enabled: boolean;
  onInput: (updatedValue: string) => void;
}

export function DataEntry(props: DataEntryProps) {
  return (
    <div className="dataEntryContainer">
      <div className="fieldName">
        <div>{props.title}</div>
      </div>
      <div className="fieldValue">
        <input
          type="text"
          inputMode={props.inputMode ?? 'text'}
          className="dataEntryInput"
          disabled={!props.enabled}
          defaultValue={props.initialValue ?? ''}
          onInput={e => props.onInput(e.currentTarget.value)}
        />
      </div>
    </div>
  );
}
