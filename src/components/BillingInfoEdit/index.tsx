import { AxiosError } from 'axios';
import React, { useEffect, useState } from 'react';

import Spinner from 'react-bootstrap/Spinner';
import { DataEntry } from '../DataEntry';
import { WebsiteButton } from '../WebsiteButton';

import './index.css';

interface ConfigEntry {
  title: string;
  inputMode?: 'none' | 'text' | 'tel' | 'url' | 'email' | 'numeric' | 'decimal' | 'search';
}

const CONFIG_LOOKUP: Record<keyof Response.BillingInfo, ConfigEntry> = {
  firstName: { title: 'First Name' },
  lastName: { title: 'Last Name' },
  email: { title: 'Email', inputMode: 'email' },
  phone: { title: 'Phone Number', inputMode: 'tel' },
  cardNumber: { title: 'Card Number', inputMode: 'numeric' },
  expMonth: { title: 'Exp. Month', inputMode: 'numeric' },
  expYear: { title: 'Exp. Year', inputMode: 'numeric' },
  zipCode: { title: 'Zip Code', inputMode: 'numeric' },
};
type Error = { code?: string; message?: string };

type BillingInfoEditProps = {
  originalBillingInfo: Response.BillingInfo;
  onSaveClicked: (billingInfo: Response.BillingInfo) => void | Promise<void>;
  onDeleteClicked: () => void | Promise<void>;
};

export default function BillingInfoEdit({
  originalBillingInfo,
  onSaveClicked,
  onDeleteClicked,
}: BillingInfoEditProps) {
  const [disableForm, setDisableForm] = useState(false);
  const [error, setError] = useState<Error>({});

  const [updatedInfo, setUpdatedInfo] = useState<Response.BillingInfo>(originalBillingInfo);
  useEffect(() => {
    setUpdatedInfo(originalBillingInfo);
  }, [originalBillingInfo]);

  return (
    <div>
      <div className="billingEditTitleContainer">
        <div>Billing Info</div>
        <div className="billingEditSpinnerContainer">
          <Spinner hidden={!disableForm} animation="border" role="status" />
        </div>
      </div>
      <div>
        {Object.entries(CONFIG_LOOKUP).map(([key, config], i) => {
          const defaultValue = originalBillingInfo[key as keyof Response.BillingInfo];
          return (
            <DataEntry
              key={`billing-info-section-${i}`}
              title={`${config.title}:`}
              inputMode={config.inputMode}
              initialValue={defaultValue}
              enabled={!disableForm}
              onInput={updatedValue => {
                setUpdatedInfo({ ...updatedInfo, [key]: updatedValue });
              }}
            />
          );
        })}
      </div>
      <div className="billingInfoSubmissionContainer">
        <div className="billingDisclaimer">
          WARNING: You are trusting someone who you know drinks copious amounts of alcohol to
          safeguard your data. He's taken as many precautions as possible (AES-256 at-rest
          encryption, an SSL connection when communicating from the website to the API, and a
          non-trivial database password to name a few), but cannot 100% guarantee that your data
          will always be safe. You are aware of the risks and are ok with supplying your billing
          info to this service.
        </div>
        {error.code ? (
          <div className="billingInfoEditErrorDisplayContainer">
            <p>{`Code: ${error.code}`}</p>
            <p>{`Message: ${error.message}`}</p>
          </div>
        ) : null}
        <div className="billingInfoButtonContainer">
          <div className="scaryButtons">
            <WebsiteButton
              text="Delete"
              type="negative"
              onClick={async () => {
                setError({});
                setDisableForm(true);
                try {
                  await onDeleteClicked();
                } catch (err) {
                  const { status, data } = (err as AxiosError).response!;
                  setError({
                    code: `${status}`,
                    message: data?.error ?? 'Unexpected error occurred',
                  });
                } finally {
                  setDisableForm(false);
                }
              }}
            />
          </div>
          <div className="nonScaryButtons">
            <WebsiteButton
              text="Save"
              type="positive"
              enabled={!disableForm && hasInfoChanged(originalBillingInfo, updatedInfo)}
              onClick={async () => {
                setError({});
                setDisableForm(true);
                try {
                  await onSaveClicked(updatedInfo);
                } catch (err) {
                  const { status, data } = (err as AxiosError).response!;
                  setError({
                    code: `${status}`,
                    message: data?.error ?? 'Unexpected error occurred',
                  });
                } finally {
                  setDisableForm(false);
                }
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

function hasInfoChanged(
  originalInfo: Response.BillingInfo,
  updatedInfo: Response.BillingInfo,
): boolean {
  const allEntries = Object.entries(originalInfo);
  return (
    allEntries.every(([key]) => !!updatedInfo[key as keyof Response.BillingInfo]) &&
    allEntries.some(([key, originalValue]) => {
      const updatedValue = updatedInfo[key as keyof Response.BillingInfo];
      return !!updatedValue && updatedValue !== originalValue;
    })
  );
}
