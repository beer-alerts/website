import React, { SyntheticEvent, useState } from 'react';

import { Accordion } from 'react-bootstrap';
import { ExpandMore, ExpandLess, AddCircleOutline, RemoveCircleOutline } from '@material-ui/icons';

import './index.css';
import './ItemWithVariants.css';

import { ItemDisplayProps } from './constants';
import { itemTintedByAvailability } from './utils';

export function ItemWithVariants({
  item: { name, variants },
  displayType,
  onClick,
}: ItemDisplayProps) {
  const [opened, setOpened] = useState(false);

  const onButtonClick = (e: SyntheticEvent) => {
    e.stopPropagation();
    onClick();
  };
  return (
    <Accordion className="itemContainer">
      <Accordion.Toggle
        eventKey={name}
        as="div"
        className="itemInnerContainer"
        onClick={() => setOpened(!opened)}
      >
        <div className="itemContentContainer">
          {displayType === 'add' ? (
            <AddCircleOutline className="icon addActionIcon" onClick={onButtonClick} />
          ) : (
            <RemoveCircleOutline className="icon removeActionIcon" onClick={onButtonClick} />
          )}
          <div>
            {name}
            <div className="statusPreviewContainer">{buildPreviewStatus(variants)}</div>
          </div>
        </div>
        <div className="expansionIcon">{opened ? <ExpandLess /> : <ExpandMore />}</div>
      </Accordion.Toggle>
      <Accordion.Collapse eventKey={name}>
        <div className="variantContainer">
          {variants.map(({ name, status }, i) => {
            const statusSpan = itemTintedByAvailability(name, status);
            return (
              <div className="variantStatus" key={i}>
                - {statusSpan}
              </div>
            );
          })}
        </div>
      </Accordion.Collapse>
    </Accordion>
  );
}

function buildPreviewStatus(variants: Display.Variant[]) {
  const { available, limited, out } = variants.reduce(
    (acc, variant) => {
      switch (variant.status) {
        case 'Available':
          acc.available++;
          break;
        case 'Supplies Low':
          acc.limited++;
          break;
        case 'Not Available':
          acc.out++;
          break;
      }
      return acc;
    },
    { available: 0, limited: 0, out: 0 },
  );

  const previewElements = [];
  if (available > 0) {
    previewElements.push(itemTintedByAvailability(`${available} available`, 'Available'));
  }
  if (limited > 0) {
    previewElements.push(itemTintedByAvailability(`${limited} limited`, 'Supplies Low'));
  }
  if (out > 0) {
    previewElements.push(itemTintedByAvailability(`${out} not available`, 'Not Available'));
  }

  return previewElements.reduce((prev, cur) => (
    <>
      {prev}, {cur}
    </>
  ));
}
