export type ItemDisplayProps = {
  item: Display.Beer;
  displayType: 'add' | 'remove';
  onClick: () => void;
};
