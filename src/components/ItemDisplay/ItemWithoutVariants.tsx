import { AddCircleOutline, RemoveCircleOutline } from '@material-ui/icons';
import React, { SyntheticEvent } from 'react';

import { ItemDisplayProps } from './constants';

import './index.css';
import { itemTintedByAvailability } from './utils';

export function ItemWithoutVariants({
  item: { name, status },
  displayType,
  onClick,
}: ItemDisplayProps) {
  const onButtonClick = (e: SyntheticEvent) => {
    e.stopPropagation();
    onClick();
  };

  return (
    <div className="itemContainer">
      <div className="itemInnerContainer">
        <div className="itemContentContainer">
          {displayType === 'add' ? (
            <AddCircleOutline className="icon addActionIcon" onClick={onButtonClick} />
          ) : (
            <RemoveCircleOutline className="icon removeActionIcon" onClick={onButtonClick} />
          )}
          <div>
            {name}
            <div className="statusPreviewContainer">{itemTintedByAvailability(status, status)}</div>
          </div>
        </div>
      </div>
    </div>
  );
}
