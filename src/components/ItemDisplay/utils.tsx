import React from 'react';

import './index.css';

export function itemTintedByAvailability(text: string, productStatus: Display.ProductStatus) {
  switch (productStatus) {
    case 'Available':
      return <span className="itemAvailableColor">{text}</span>;
    case 'Supplies Low':
      return <span className="itemLowStockColor">{text}</span>;
    case 'Not Available':
      return <span className="itemUnavailableColor">{text}</span>;
  }
}
