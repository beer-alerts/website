import React, { useContext } from 'react';
import { Redirect, Route, RouteProps, useLocation } from 'react-router-dom';
import { AuthDataContext } from './AuthDataProvider';
import { LocationDescriptor } from 'history';
import { LoggedInContainer } from './LoggedInContainer';

export const TEXT_COLOR = '#CCCCCC';

export function PrivateRoute<T extends RouteProps>({ component: Component, ...rest }: T) {
  const location = useLocation();
  const authData = useContext(AuthDataContext);
  return (
    <Route
      {...rest}
      render={props => {
        if (authData?.userId && Component) {
          return (
            <LoggedInContainer>
              <Component {...props} />
            </LoggedInContainer>
          );
        }
        return (
          <Redirect
            to={
              {
                pathname: '/login',
                state: { pathname: location.pathname, search: location.search },
              } as LocationDescriptor<RouteAfterAuth>
            }
          />
        );
      }}
    />
  );
}
