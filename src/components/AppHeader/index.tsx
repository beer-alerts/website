import React from 'react';

import { Close, Menu } from '@material-ui/icons';

import './AppHeader.css';
import { useAuthData } from '../AuthDataProvider';
import { Logo } from '../Logo';
import { useHistory } from 'react-router';

const menuIconStyle: React.CSSProperties = {
  height: '30px',
  width: '30px',
};

interface SideMenuToggleProps {
  isMenuOpen: boolean;
  onToggle: (isOpen: boolean) => void;
}

export function AppHeader({ isMenuOpen, onToggle }: SideMenuToggleProps) {
  const { beerBuddy } = useAuthData();
  const history = useHistory();

  return (
    <div className="appHeaderContainer">
      <div className="menuButton start">
        {isMenuOpen ? (
          <Close style={menuIconStyle} onClick={() => onToggle(false)} />
        ) : (
          <Menu style={menuIconStyle} onClick={() => onToggle(true)} />
        )}
      </div>
      <div className="logoDisplay">
        <Logo
          onClick={() => {
            history.push('/');
          }}
        />
      </div>
      <div className="nameDisplay end">
        <span>{beerBuddy?.name ? `Hello ${beerBuddy.name}!` : 'Hello'}</span>
      </div>
    </div>
  );
}
