import React, { createContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

import * as firebase from 'firebase/auth';

import store from 'store';
import { GET } from '../api';

interface BasicUserInfo {
  id: string;
  name?: string | null;
  email?: string | null;
}

export interface AuthData {
  userId?: string | null;
  firebaseUser?: BasicUserInfo | null;
  beerBuddy?: Response.User | null;
  authState: AuthState;

  login: () => Promise<void>;
  updateUser: (user: Response.User) => Promise<void>;
  getBearerToken: () => Promise<string>;
  logout: () => Promise<void>;
}

const INITIAL_STATE: () => AuthData = () => ({
  userId: store.get('userId'),
  authState: 'LOGGED_OUT',

  login: async () => {},
  updateUser: async () => {},
  getBearerToken: async () => '',
  logout: async () => {},
});

export const AuthDataContext = createContext<AuthData>(INITIAL_STATE());
export function useAuthData() {
  const context = React.useContext(AuthDataContext);
  if (context === undefined) {
    throw new Error('useAuthData must be used within a AuthDataProvider');
  }
  return context;
}
const AuthDataProvider: React.FC = ({ children }) => {
  const [firebaseUser, setFirebaseUser] = useState<firebase.User | null>(null);
  const [authData, setAuthData] = useState<AuthData>(INITIAL_STATE());
  const history = useHistory();

  useEffect(
    () =>
      firebase.getAuth().onAuthStateChanged(user => {
        if (!user && store.get('firebaseUid')) {
          return;
        }
        if (user) {
          store.set('firebaseUid', user.uid);
          setAuthData(oldAuthData => ({
            ...oldAuthData,
            authState: 'ATTEMPTING_BA_LOGIN',
            firebaseUser: {
              id: user.uid,
              name: user.displayName,
              email: user.email,
            },
          }));
        }
        setFirebaseUser(user);
      }),
    [],
  );

  useEffect(() => {
    (async () => {
      if (!firebaseUser) {
        setAuthData(INITIAL_STATE());
      } else {
        const { user, newAuthState }: { user: Response.User; newAuthState: AuthState } = await GET<
          Response.User
        >('/auth', {
          bearerToken: await firebaseUser.getIdToken(),
        })
          .then(user => ({ user, newAuthState: 'LOGGED_IN' as AuthState }))
          .catch(err => {
            if (err.response?.status !== 404) {
              throw err;
            }
            return { user: {} as Response.User, newAuthState: 'NEEDS_INFO' as AuthState };
          });
        store.set('userId', user.id);
        setAuthData(oldAuthData => ({
          ...oldAuthData,
          authState: newAuthState,
          userId: user.id,
          beerBuddy: user,
        }));
      }
    })();
  }, [firebaseUser]);

  return (
    <AuthDataContext.Provider
      value={{
        ...authData,
        login: async () => {
          const auth = firebase.getAuth();
          await auth.signOut();
          await firebase.signInWithPopup(auth, new firebase.GoogleAuthProvider());
        },
        updateUser: async (user: Response.User) => {
          store.set('userId', user.id);
          setAuthData(oldAuthData => ({
            ...oldAuthData,
            authState: 'LOGGED_IN',
            beerBuddy: user,
            userId: user.id,
          }));
        },
        getBearerToken: async () => firebaseUser?.getIdToken() ?? '',
        logout: async () => {
          store.remove('userId');
          store.remove('firebaseUid');
          history.replace({ pathname: '/', state: null });
          await firebase.getAuth().signOut();
        },
      }}
    >
      {children}
    </AuthDataContext.Provider>
  );
};
export default AuthDataProvider;
