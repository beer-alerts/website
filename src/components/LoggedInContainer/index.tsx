import React, { useState } from 'react';
import { AppHeader } from '../AppHeader';
import { SideMenu } from '../SideMenu';

import './LoggedInContainer.css';

export function LoggedInContainer({ children }: React.PropsWithChildren<unknown>) {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  return (
    <div className="loggedInContainer">
      <AppHeader isMenuOpen={isMenuOpen} onToggle={shouldOpen => setIsMenuOpen(shouldOpen)} />
      <SideMenu onDismiss={() => setIsMenuOpen(false)} isOpen={isMenuOpen} />
      <div className="contentContainer">{children}</div>
    </div>
  );
}
