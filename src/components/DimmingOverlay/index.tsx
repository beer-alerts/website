import React from 'react';

import './DimmingOverlay.css';

interface SideMenuOverlayProps {
  isOpen: boolean;
  overlayTapped: () => void;
}

export default function DimmingOverlay({
  isOpen,
  overlayTapped,
  children,
}: React.PropsWithChildren<SideMenuOverlayProps>) {
  return (
    <div
      className={`sideMenuOverlay ${isOpen ? 'open' : 'closed'}`}
      onClick={e => {
        e.preventDefault();
        overlayTapped();
      }}
    >
      {children}
    </div>
  );
}
