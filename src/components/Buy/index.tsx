import React, { useEffect, useState } from 'react';
import ReactHtmlParser from 'react-html-parser';

import { useAuthData } from '../AuthDataProvider';
import { GET, GET_WITH_STATUS, POST } from '../../api';
import { useLocation } from 'react-router';
import { WebsiteButton } from '../WebsiteButton';
import { SearchBar } from '../SearchBar';

import './Buy.css';

const BLANK_STATE = {
  firstName: '',
  lastName: '',
  phone: '',
  email: '',
  cardNumber: '',
  expMonth: '',
  expYear: '',
  zipCode: '',
};

enum DisclaimerState {
  NotShown,
  Showing,
  Shown,
}

export default function Buy() {
  const queryParams = new URLSearchParams(useLocation().search);
  const { userId, beerBuddy, getBearerToken } = useAuthData();

  const [billingInfo, setBillingInfo] = useState<Response.BillingInfo | null>(null);
  const [currentInventory, setCurrentInventory] = useState<Response.Product[]>([]);
  const [searchPhrase, setSearchPhrase] = useState('');
  const [filteredInventory, setFilteredInventory] = useState<Response.Product[]>([]);
  const [currentProductId, setCurrentProductId] = useState<string>(
    queryParams.get('productId') ?? '',
  );
  const [currentProductDescription, setCurrentProductDescription] = useState('');
  const [quantity, setQuantity] = useState(queryParams.get('quantity') ?? '1');
  const [cvv, setCVV] = useState<string>(queryParams.get('cvv') ?? '');
  const [hasValidData, setHasValidData] = useState(false);
  const [disclaimerState, setDisclaimerState] = useState(DisclaimerState.NotShown);
  const [isPurchasing, setIsPurchasing] = useState(false);

  useEffect(() => {
    (async () => {
      if (userId && beerBuddy) {
        const { statusCode, data: currentBillingInfo } = await GET_WITH_STATUS<
          Response.BillingInfo
        >(`/users/${userId}/billing`, {
          bearerToken: await getBearerToken(),
        });
        if (statusCode === 204) {
          setBillingInfo(BLANK_STATE);
        } else {
          setBillingInfo(currentBillingInfo);
        }
      }
    })();
  }, [beerBuddy, getBearerToken, userId]);

  useEffect(() => {
    (async () => {
      if (hasValidBillingInfo(billingInfo)) {
        const { currentInventory } = await GET<Response.Brewery>('/breweries/hm', {
          bearerToken: await getBearerToken(),
        });
        setCurrentInventory(currentInventory.sort((a, b) => a.name.localeCompare(b.name)));
      }
    })();
  }, [billingInfo, getBearerToken]);

  useEffect(() => {
    const isSearching = searchPhrase.length > 0;
    setFilteredInventory(
      isSearching
        ? currentInventory.filter(({ name }) =>
            name.toLocaleLowerCase().startsWith(searchPhrase.toLocaleLowerCase()),
          )
        : currentInventory,
    );
  }, [searchPhrase, currentInventory]);

  useEffect(() => {
    const isFormValid =
      currentInventory.some(({ productId }) => currentProductId === productId) &&
      Number.parseInt(quantity) > 0 &&
      cvv.length >= 3;
    if (isFormValid && disclaimerState === DisclaimerState.NotShown) {
      setDisclaimerState(DisclaimerState.Showing);
    } else if (!isFormValid && disclaimerState === DisclaimerState.Showing) {
      setDisclaimerState(DisclaimerState.Shown);
    }
    setHasValidData(isFormValid);
  }, [currentInventory, currentProductId, cvv, disclaimerState, quantity]);

  useEffect(() => {
    if (currentProductId.length > 0) {
      const { description = '' } =
        currentInventory.find(({ productId }) => currentProductId === productId) ?? {};
      setCurrentProductDescription(description);
    } else {
      setCurrentProductDescription('');
    }
  }, [currentInventory, currentProductId]);

  if (!billingInfo) {
    return null;
  }

  if (!hasValidBillingInfo(billingInfo)) {
    return (
      <div className="NoCCInfoWarningBanner">
        Please enter your credit card info before trying to use this page
      </div>
    );
  }

  return !currentInventory.length ? null : (
    <div className="buyMainContainer">
      <div className="buyDescription">
        Select the beer you want to buy{' '}
        <span className="productId">(Product id shown in parentheses)</span>:
      </div>
      <SearchBar placeholder="Search current inventory" onInput={setSearchPhrase} />
      <div className="productInfoContainer">
        {filteredInventory.map((item, i) => (
          <div
            className="productDisplay"
            key={`product-info-${i}`}
            onClick={() => {
              setCurrentProductId(item.productId);
            }}
          >
            <div className="productName">
              {item.name} <span className="productId">({item.productId})</span>
            </div>
          </div>
        ))}
      </div>
      <div className="purchaseContainer">
        <div className="purchaseActionContainer">
          <div className="purchaseEntryField">
            <label>Product Id</label>
            <input
              type="text"
              inputMode="numeric"
              value={currentProductId}
              onChange={({ currentTarget }) => setCurrentProductId(currentTarget.value)}
            />
          </div>
          <div className="purchaseEntryField">
            <label>Quantity</label>
            <input
              type="text"
              inputMode="numeric"
              value={quantity}
              onChange={({ currentTarget }) => setQuantity(currentTarget.value)}
            />
          </div>
          <div className="purchaseEntryField rightmost">
            <label>CVV</label>
            <input
              type="text"
              inputMode="numeric"
              value={cvv}
              onChange={({ currentTarget }) => setCVV(currentTarget.value)}
            />
          </div>
          <div className="purchaseButton">
            <WebsiteButton
              text="Purchase"
              type="positive"
              enabled={hasValidData && !isPurchasing}
              onClick={async () => {
                setIsPurchasing(true);
                try {
                  await POST<Response.Empty>('/purchase', {
                    body: { userId, productId: currentProductId, quantity, cvv },
                    bearerToken: await getBearerToken(),
                  });
                  alert('Purchase successful, look for an official email from Holy Mountain!');
                  setCurrentProductId('');
                  setQuantity('1');
                  setCVV('');
                } catch {
                  alert('An unexpected error occurred trying to make this purchase.');
                } finally {
                  setIsPurchasing(false);
                }
              }}
            />
          </div>
        </div>
        {determineAdditionalInfoContainer(disclaimerState, currentProductDescription)}
      </div>
    </div>
  );
}

function hasValidBillingInfo(billingInfo: Response.BillingInfo | null): boolean {
  return billingInfo ? Object.values(billingInfo).every(value => !!value) : false;
}

function determineAdditionalInfoContainer(
  disclaimerState: DisclaimerState,
  currentProductDescription: string,
) {
  if (disclaimerState === DisclaimerState.Showing) {
    return (
      <div className="additionalInfoContainer disclaimer">
        WARNINGS:
        <br />
        - You are responsible for adhering to any and all bottle limits listed in the beer
        descriptions. These are pulled directly from Holy Mountain so should be accurate and assumed
        to be enforced by them.
        <br />
        <br />- There is NO chance to change your order once you press "Purchase"; assuming your CVV
        is valid you WILL be charged whatever the appropriate price is for your selection multiplied
        by the quantity. Additionally, a 20% tip will be applied to all purchases with no option to
        change it. If for some reason you'd prefer to tip less, submit a feature request.
      </div>
    );
  }
  if (currentProductDescription.length > 0) {
    return (
      <div className="additionalInfoContainer">{ReactHtmlParser(currentProductDescription)}</div>
    );
  }
  return null;
}
