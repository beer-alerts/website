import { GET } from '../../api';

type InventoryLookup = { [key: string]: Response.Product };

export default async function getSubscriptionManagementData(
  userId: string | number,
  breweryId: string,
  bearerToken: string,
): Promise<Display.SubscriptionManagement> {
  const [user, brewery] = await Promise.all([
    GET<Response.User>(`/users/${userId}`, { bearerToken }),
    GET<Response.Brewery>(`/breweries/${breweryId}`, { bearerToken }),
  ]);

  const inventoryLookup = brewery.currentInventory.reduce((acc: InventoryLookup, product) => {
    acc[product.productCode] = product;

    return acc;
  }, {});
  const currentUserSubscriptions = user.subscriptions.reduce((acc, { topic }) => {
    acc.add(topic.productCode);
    return acc;
  }, new Set());
  const userSubscriptions = user.subscriptions.map(({ topic }) =>
    transformBeerFamily(topic, inventoryLookup),
  );
  const availableSubscriptions = brewery.beers
    .filter(({ productCode }) => !currentUserSubscriptions.has(productCode))
    .map(beer => transformBeerFamily(beer, inventoryLookup));

  return {
    user: {
      id: user.id,
      name: user.name,
      email: user.email,
      subscriptions: orderSubscriptionInformation(userSubscriptions),
    },
    currentBrewery: {
      name: brewery.name,
      beers: orderSubscriptionInformation(availableSubscriptions),
    },
  };
}

function transformBeerFamily(
  beerFamily: Response.BeerFamily,
  inventoryLookup: InventoryLookup,
): Display.Beer {
  const { status } = inventoryLookup[beerFamily.productCode] || {};

  return {
    id: beerFamily.id,
    name: beerFamily.name,
    status: transformStatus(status),
    variants: beerFamily.beers.map(beer => transformBeer(beer, inventoryLookup)),
  };
}

function transformBeer(beer: Response.Beer, inventoryLookup: InventoryLookup): Display.Variant {
  const { status } = inventoryLookup[beer.productCode] || {};

  return { name: beer.name, status: transformStatus(status) };
}

function transformStatus(status?: Response.ProductStatus): Display.ProductStatus {
  switch (status) {
    case 'in':
      return 'Available';
    case 'limited':
      return 'Supplies Low';
    case 'out':
    default:
      return 'Not Available';
  }
}

const STATUS_ORDER_LOOKUP: { [key in Display.ProductStatus]: number } = {
  Available: 0,
  'Supplies Low': 1,
  'Not Available': 2,
};

function orderSubscriptionInformation(beers: Display.Beer[]): Display.Beer[] {
  return sortBeers(beers).map(beer => ({
    ...beer,
    variants: sortVariants(beer.variants),
  }));
}

function sortBeers(beers: Display.Beer[]): Display.Beer[] {
  return beers.sort((a, b) => {
    const aStatusOrder = STATUS_ORDER_LOOKUP[getBeerStatus(a)];
    const bStatusOrder = STATUS_ORDER_LOOKUP[getBeerStatus(b)];
    if (aStatusOrder < bStatusOrder) {
      return -1;
    }
    if (bStatusOrder < aStatusOrder) {
      return 1;
    }

    return a.name.localeCompare(b.name);
  });
}

function getBeerStatus(beer: Display.Beer): Display.ProductStatus {
  if (beer.variants.length > 0) {
    return beer.variants.reduce((best, variant) =>
      STATUS_ORDER_LOOKUP[variant.status] < STATUS_ORDER_LOOKUP[best.status] ? variant : best,
    ).status;
  }

  return beer.status;
}

function sortVariants(variants: Display.Variant[]): Display.Variant[] {
  return variants.sort((a, b) => {
    const aStatusOrder = STATUS_ORDER_LOOKUP[a.status];
    const bStatusOrder = STATUS_ORDER_LOOKUP[b.status];
    if (aStatusOrder < bStatusOrder) {
      return -1;
    }
    if (bStatusOrder < aStatusOrder) {
      return 1;
    }

    return a.name.localeCompare(b.name);
  });
}
