import React, { useEffect, useState } from 'react';
import { DELETE, PUT } from '../../api';
import getSubscriptionManagementData from './request';

import { useAuthData } from '../AuthDataProvider';
import { ItemWithVariants } from '../ItemDisplay/ItemWithVariants';
import { ItemWithoutVariants } from '../ItemDisplay/ItemWithoutVariants';
import { ItemDisplayProps } from '../ItemDisplay/constants';

import './SubscriptionManagement.css';
import { SearchBar } from '../SearchBar';

export default function SubscriptionManagement() {
  const { userId, beerBuddy, getBearerToken } = useAuthData();

  const [needsRefresh, setNeedsRefresh] = useState(true);
  const [{ user, currentBrewery }, setData] = useState<Display.SubscriptionManagement>({});
  const [searchPhrase, setSearchPhrase] = useState('');
  const [filteredSubscriptions, setFilteredSubscriptions] = useState<Display.Beer[]>([]);
  const [filteredBeers, setFilteredBeers] = useState<Display.Beer[]>([]);

  useEffect(() => {
    (async () => {
      if (userId && beerBuddy && needsRefresh) {
        setNeedsRefresh(false);
        setData(await getSubscriptionManagementData(userId, 'hm', await getBearerToken()));
      } else {
        setData({});
      }
    })();
  }, [userId, beerBuddy, getBearerToken, needsRefresh]);

  useEffect(() => {
    if (!user || !currentBrewery) {
      return;
    }
    setFilteredSubscriptions(filterBeers(searchPhrase, user.subscriptions));
    setFilteredBeers(filterBeers(searchPhrase, currentBrewery.beers));
  }, [currentBrewery, searchPhrase, user]);

  return (
    <>
      {user && currentBrewery && (
        <>
          <SearchBar placeholder="Search for beers" onInput={setSearchPhrase} />
          <div className="subscriptionManagementContainer">
            <div className="subscriptionSectionContainer">
              <div className="sectionTitle left">Your Subscriptions</div>
              <div className="subscriptionSectionContentContainer">
                {filteredSubscriptions.map((beer, i) => {
                  const props: ItemDisplayProps = {
                    item: beer,
                    displayType: 'remove',
                    onClick: async () => {
                      await DELETE<Response.Empty>(`/users/${user?.id}/subscriptions/${beer.id}`, {
                        bearerToken: await getBearerToken(),
                      });
                      setNeedsRefresh(true);
                    },
                  };
                  return beer.variants.length > 0 ? (
                    <ItemWithVariants key={i} {...props} />
                  ) : (
                    <ItemWithoutVariants key={i} {...props} />
                  );
                })}
              </div>
            </div>
            <div className="subscriptionSectionContainer">
              <div className="sectionTitle right">Available Subscriptions</div>
              <div className="subscriptionSectionContentContainer">
                {filteredBeers.map((beer, i) => {
                  const props: ItemDisplayProps = {
                    item: beer,
                    displayType: 'add',
                    onClick: async () => {
                      await PUT<Response.Subscription>(
                        `/users/${user?.id}/subscriptions/${beer.id}`,
                        {
                          bearerToken: await getBearerToken(),
                        },
                      );
                      setNeedsRefresh(true);
                    },
                  };
                  return beer.variants.length > 0 ? (
                    <ItemWithVariants key={i} {...props} />
                  ) : (
                    <ItemWithoutVariants key={i} {...props} />
                  );
                })}
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
}

function filterBeers(searchPhrase: string, beersToSearch: Display.Beer[]) {
  const isSearching = searchPhrase.length > 0;
  return isSearching
    ? beersToSearch.filter(({ name }) =>
        name.toLocaleLowerCase().startsWith(searchPhrase.toLocaleLowerCase()),
      )
    : beersToSearch;
}
