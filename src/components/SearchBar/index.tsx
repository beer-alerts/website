import { Search } from '@material-ui/icons';
import React from 'react';

import './SearchBar.css';

interface SearchBarProps<T> {
  placeholder: string;
  onInput: (phrase: string) => void;
}

export function SearchBar<T>(props: SearchBarProps<T>) {
  return (
    <div className="searchBarContainer">
      <div className="searchIconContainer">
        <Search />
      </div>
      <input
        type="text"
        className="searchBarInput"
        placeholder={props.placeholder}
        onInput={e => props.onInput(e.currentTarget.value)}
      />
      <div />
    </div>
  );
}
