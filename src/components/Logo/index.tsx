import React from 'react';

import logoImage from './logo.png';
import './Logo.css';

export function Logo({ onClick }: { onClick: () => void }) {
  return (
    <div className="logoContainer" onClick={onClick}>
      <img className="logoImage" src={logoImage} alt={'Beer Alerts Logo'} />
      <div className="logoText">Beer Alerts</div>
    </div>
  );
}
