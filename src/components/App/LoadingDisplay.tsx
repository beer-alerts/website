import React from 'react';

import './LoadingDisplay.css';

export default function LoadingDisplay({ message }: { message?: string }) {
  return <div className="loadingDisplay">{message ?? 'Loading...'}</div>;
}
