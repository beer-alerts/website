import React from 'react';
import { Switch, Route } from 'react-router-dom';
import './index.css';
import { PrivateRoute } from '../BasicComponents';

const Login = React.lazy(() => import('../Login'));
const SubscriptionManagement = React.lazy(() => import('../SubscriptionManagement'));
const Buy = React.lazy(() => import('../Buy'));
const UserInfoEdit = React.lazy(() => import('../UserInfoEdit'));

export default function App() {
  return (
    <div className="appContainer">
      <Switch>
        <Route exact path="/login" component={Login} />
        <PrivateRoute exact path="/buy" component={Buy} />
        <PrivateRoute exact path="/profile" component={UserInfoEdit} />
        <PrivateRoute exact path="/" component={SubscriptionManagement} />
      </Switch>
    </div>
  );
}
