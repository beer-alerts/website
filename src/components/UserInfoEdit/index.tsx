import React, { useEffect, useState } from 'react';
import { DELETE, GET_WITH_STATUS, PATCH, PUT } from '../../api';
import { useAuthData } from '../AuthDataProvider';
import BillingInfoEdit from '../BillingInfoEdit';
import ProfileEdit from '../ProfileEdit';

import './UserInfoEdit.css';

const BLANK_BILLING_STATE: Response.BillingInfo = {
  firstName: '',
  lastName: '',
  phone: '',
  email: '',
  cardNumber: '',
  expMonth: '',
  expYear: '',
  zipCode: '',
};

export default function UserInfoEdit() {
  const { userId, beerBuddy, getBearerToken, updateUser } = useAuthData();
  const [billingInfo, setBillingInfo] = useState<Response.BillingInfo | null>(null);

  useEffect(() => {
    (async () => {
      if (userId && beerBuddy && !billingInfo) {
        const { statusCode, data: currentBillingInfo } = await GET_WITH_STATUS<
          Response.BillingInfo
        >(`/users/${userId}/billing`, { bearerToken: await getBearerToken() });
        if (statusCode === 204) {
          setBillingInfo(BLANK_BILLING_STATE);
        } else {
          setBillingInfo(currentBillingInfo);
        }
      }
    })();
  }, [beerBuddy, billingInfo, getBearerToken, userId]);
  return (
    <>
      {beerBuddy && billingInfo && (
        <div className="userInfoMainContainer">
          <ProfileEdit
            originalDisplayName={beerBuddy.name ?? ''}
            originalEmail={beerBuddy.email ?? ''}
            onSaveClicked={async userInfo => {
              const updatedUser = await PATCH<Response.User>(`/users/${beerBuddy.id}`, {
                bearerToken: await getBearerToken(),
                body: userInfo,
              });
              updateUser(updatedUser);
            }}
          />
          <BillingInfoEdit
            originalBillingInfo={billingInfo}
            onSaveClicked={async updatedInfo => {
              await PUT<Response.BillingInfo>(`/users/${userId}/billing`, {
                body: updatedInfo,
                bearerToken: await getBearerToken(),
              });
              setBillingInfo(updatedInfo);
            }}
            onDeleteClicked={async () => {
              await DELETE<Response.Empty>(`/users/${userId}/billing`, {
                bearerToken: await getBearerToken(),
              });
              setBillingInfo(BLANK_BILLING_STATE);
            }}
          />
        </div>
      )}
    </>
  );
}
