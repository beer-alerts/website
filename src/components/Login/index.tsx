import React, { useContext, useEffect, useState } from 'react';

import './index.css';

import { POST } from '../../api';
import { Redirect, useLocation } from 'react-router-dom';
import { AuthDataContext } from '../AuthDataProvider';
import RegistrationDisplay from './RegistrationDisplay';
import { WebsiteButton } from '../WebsiteButton';

export default function Login() {
  const { firebaseUser, authState, login, updateUser, getBearerToken } = useContext(
    AuthDataContext,
  );
  const location = useLocation<RouteAfterAuth>();
  const [redirectAfterLogin, setRedirectAfterLogin] = useState<RouteAfterAuth>({ pathname: '/' });

  useEffect(() => {
    setRedirectAfterLogin({
      pathname: location.state?.pathname ?? '',
      search: location.state?.search,
    });
  }, [location.state]);

  switch (authState) {
    case 'LOGGED_IN':
      return <Redirect to={redirectAfterLogin ?? '/'} />;
    case 'NEEDS_INFO':
      return (
        <div className="loginMainContainer">
          <div className="loginContentContainer">
            <div className="loginTitleContainer">We Need a Little More Info</div>
            <RegistrationDisplay
              initialRegistrationData={{
                name: firebaseUser?.name,
                email: firebaseUser?.email,
              }}
              onSubmit={async newUser => {
                const user = await POST<Response.User>('/users', {
                  bearerToken: await getBearerToken(),
                  body: { ...newUser, authProviderId: firebaseUser!.id },
                });
                updateUser(user);
              }}
            />
          </div>
        </div>
      );
    default:
      return (
        <div className="loginMainContainer">
          <div className="loginContentContainer">
            <div className="loginTitleContainer">Welcome to Beer Alerts!</div>
            <WebsiteButton text="Sign In" type="positive" onClick={login} />
          </div>
        </div>
      );
  }
}
