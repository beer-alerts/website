import React, { useState } from 'react';

import './RegistrationDisplay.css';

type NewUserData = { name?: string | null; email?: string | null };

interface RegistrationDisplayProps {
  initialRegistrationData: NewUserData;
  onSubmit: (user: NewUserData) => void;
}

export default function RegistrationDisplay({
  initialRegistrationData,
  onSubmit,
}: RegistrationDisplayProps) {
  const [newUserData, setNewUserData] = useState<NewUserData>(initialRegistrationData);

  return (
    <form
      className="registrationContainer"
      onSubmit={async e => {
        e.preventDefault();
        onSubmit(newUserData);
      }}
    >
      <div className="registrationSectionContainer">
        <label>Display Name:</label>
        <input
          type="text"
          className="registrationInput"
          defaultValue={newUserData.name || ''}
          onInput={e => {
            setNewUserData({ ...newUserData, name: e.currentTarget.value });
          }}
        />
      </div>
      <div className="registrationSectionContainer">
        <label>Email:</label>
        <input
          type="email"
          className="registrationInput"
          pattern=".+@.+"
          defaultValue={newUserData.email || ''}
          onInput={e => {
            setNewUserData({ ...newUserData, email: e.currentTarget.value });
          }}
        />
      </div>
      <div className="registrationAdditionalInfo">
        If you want these alerts sent to a phone number,{' '}
        <a
          href="https://www.dialmycalls.com/blog/send-text-messages-email-address"
          target="_blank"
          rel="noopener noreferrer"
        >
          here is a list of all major carriers' email-to-sms gateways
        </a>
      </div>
      <input
        type="submit"
        className="websiteButton registrationButton positive"
        value="Complete Registration"
        disabled={!newUserData.name || !newUserData.email}
      />
    </form>
  );
}
