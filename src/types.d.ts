declare module Response {
  type ProductStatus = 'in' | 'limited' | 'out';

  interface Brewery {
    name: string;
    currentInventory: Product[] = [];
    beers: BeerFamily[];
  }

  interface Product {
    name: string;
    productId: string;
    productCode: string;
    status: ProductStatus;
    description: string;
  }

  interface BeerFamily {
    id: string;
    name: string;
    productCode: string;
    beers: Beer[] = [];
  }

  interface Beer {
    name: string;
    productCode: string;
  }

  interface User {
    id: string;
    name: string;
    email: string;
    subscriptions: Subscription[] = [];
  }

  interface BillingInfo {
    firstName: string;
    lastName: string;
    email: string;
    phone: string;
    cardNumber: string;
    expMonth: string;
    expYear: string;
    zipCode: string;
  }

  interface Subscription {
    topic: BeerFamily;
  }

  interface Empty {
    statusCode: number;
  }
}

declare module Display {
  type ProductStatus = 'Available' | 'Supplies Low' | 'Not Available';

  interface Brewery {
    name: string;
    beers: Beer[];
  }

  interface Beer {
    id: string;
    name: string;
    variants: Variant[] = [];
    status: ProductStatus;
  }

  interface Variant {
    name: string;
    status: ProductStatus;
  }

  interface User {
    id: string;
    name: string;
    email: string;
    subscriptions: Beer[] = [];
  }

  interface SubscriptionManagement {
    user?: User;
    currentBrewery?: Brewery;
  }
}

type AuthState = 'LOGGED_OUT' | 'ATTEMPTING_BA_LOGIN' | 'NEEDS_INFO' | 'LOGGED_IN';

interface RouteAfterAuth {
  pathname: string;
  search?: string;
}
