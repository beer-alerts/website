import { getAuth } from 'firebase/auth';

import store from 'store';

export const logout = async () => {
  store.remove('currentUserId');
  await getAuth().signOut();
};
